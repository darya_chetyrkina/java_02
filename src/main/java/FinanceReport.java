import java.util.Arrays;

public class FinanceReport {
    //Создайте класс FinanceReport, содержащий массив платежей, ФИО составителя
    //отчета, дату создания отчета. Методы: получение количества платежей, доступ к iму платежу (на чтение и запись).

    private String Editor;
    private final Payment[] arrayOfPayments;
    private int day;
    private int month;
    private int year;

    public FinanceReport(String Editor, int quantity, int day, int month, int year){
        if (quantity < 0) throw new IllegalArgumentException("quantity is negative");
        setDay(day);
        setMonth(month);
        setYear(year);
        setEditor(Editor);
        this.arrayOfPayments = new Payment[quantity];


    }

    public int getQuantity() {
        return arrayOfPayments.length;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public String getEditor() { return Editor;}

    public void setDay(int day) {
        this.day = day;
    }
    public void setMonth(int month) {
        this.month = month;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public void setEditor(String name){
        if (name == null) throw new IllegalArgumentException("Name is null");
        this.Editor = name;
    }

    public Payment getPayment(int index){
        if (arrayOfPayments[index] == null) throw new IllegalArgumentException("Impossible get a payment: invalidate number");
        else return arrayOfPayments[index];
    }

    public void setPayment(int index, Payment payment){
        if (index >= 0 && index < arrayOfPayments.length && payment != null) {
            this.arrayOfPayments[index] = payment;
        }else throw new IllegalArgumentException("Impossible set a payment");
    }


    public String toString(){
        //9. Добавьте в FinanceReport метод toString, который преобразует отчет в набор строк
        //формата (используйте String.format):
        //[Автор: ФИО составителя, дата: дата.создания, Платежи: [
        // Плательщик: ФИО, дата: день.месяц.год сумма: *** руб. ** коп.\n,
        // Плательщик: ФИО, дата: день.месяц.год сумма: *** руб. ** коп.\n,… ]]

        return String.format("[Автор: %s Дата: %d.%d.%d, Платежи: %n%s]", Editor, getDay(),getMonth(),getYear(), Arrays.toString(arrayOfPayments));

    }

    public FinanceReport(FinanceReport report){
            //Добавьте конструктор копирования (после создания копии массива при изменении
            //данных в объектах исходного массива копия изменяться не должна).
            this.Editor = report.Editor;
            this.day = report.day;
            this.month = report.month;
            this.year = report.year;
            this.arrayOfPayments = new Payment[report.getQuantity()];

            for (int i = 0; i < arrayOfPayments.length; i++){

                this.arrayOfPayments[i] = new Payment(report.getPayment(i).getFullName(),report.getPayment(i).getDay(),report.getPayment(i).getMonth(),report.getPayment(i).getYear(),report.getPayment(i).getSumPayment());

            }

        }

}
