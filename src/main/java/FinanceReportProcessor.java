public class FinanceReportProcessor {

    //11. Создайте новый класс FinanceReportProcessor, в котором реализуйте статические
    //методы, все методы возвращают объект класса FinanceReport:

    public static FinanceReport getReportByCh(char ch, FinanceReport report) {
        //1) получение платежей всех людей, чья фамилия начинается на указанный символ
        //(символ — входной параметр),
        if (report == null) throw new IllegalArgumentException("Report is null");
        int count = 0;
        for (int i = 0; i < report.getQuantity(); i++) {
            if (report.getPayment(i).getFullName().indexOf(ch) == 0) {
                count++;
            }
        }

        FinanceReport report2 = new FinanceReport(report.getEditor(), count, report.getDay(), report.getMonth(), report.getYear());
        count = 0;

        for (int i = 0; i < report.getQuantity(); i++) {

            if (report.getPayment(i).getFullName().indexOf(ch) == 0) {
                report2.setPayment(count, report.getPayment(i));
                count++;
            }

        }
        return report2;


    }

    public static FinanceReport getReportBySum(FinanceReport report, int payments) {
        //2) получение всех платежей, размер которых меньше заданной величины.
        if (report == null) throw new IllegalArgumentException("Report is null");
        int count = 0;
        for (int i = 0; i < report.getQuantity(); i++) {
            if (report.getPayment(i).getSumPayment() < payments) {
                count++;
            }
        }

        FinanceReport report2 = new FinanceReport(report.getEditor(), count, report.getDay(), report.getMonth(), report.getYear());
        count = 0;
        for (int i = 0; i < report.getQuantity(); i++) {

            if (report.getPayment(i).getSumPayment() < payments) {
                report2.setPayment(count, report.getPayment(i));
                count++;
            }

        }
        return report2;
    }
}
