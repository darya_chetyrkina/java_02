import java.util.Objects;

public class Payment {
//Создайте класс Payment (платеж) с полями: ФИО человека (одна строка), дата
//платежа — число, месяц и год (целые числа), сумма платежа (целое число — сумма
//в копейках). Напишите необходимые конструкторы, геттеры/сеттеры, методы
//equals, hashCode, toString.

    private String FullName;
    private int day;
    private int month;
    private int year;
    private int sumPayment;

    Payment(String FullName, int day, int month, int year, int sumPayment){
        setSumPayment(sumPayment);
        setDay(day);
        setMonth(month);
        setYear(year);
        setFullName(FullName);
    }

    public String getFullName() {
        return FullName;
    }

    public void setSumPayment(int sumPayment) {
        if (sumPayment < 0) throw new IllegalArgumentException("Negative sum");
        this.sumPayment = sumPayment;
    }

    public int getSumPayment() {
        return sumPayment;
    }

    public void setFullName(String fullName) {
        if (fullName == null) throw new IllegalArgumentException("Name is null");
            this.FullName = fullName;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {this.day = day;}

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment = (Payment) o;
        return day == payment.day && month == payment.month && year == payment.year && sumPayment == payment.sumPayment && FullName.equals(payment.FullName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFullName(), day, month, year, getSumPayment());
    }

    public String toString(){
        return String.format("%nПлательщик: %s Дата: %d.%d.%d Сумма: %d руб. %2d коп.",FullName, day, month, year, sumPayment/100, sumPayment%100);
    }
}
