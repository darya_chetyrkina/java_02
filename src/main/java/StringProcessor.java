public class StringProcessor {

    //повторить строку
    public static String repeatNTimes(String s, int n){
        if (n < 0){
            throw new IllegalArgumentException("N is negative");
        }

        return s.repeat(n);
    }

    //вхождение второй строки в первую
    public static int countOccurrences(String str, String check){

        if (check == null || check.equals("")) {
            throw new IllegalArgumentException("Wrong second string");
        }

        int index = 0;
        int lastIndex = 0;
        int counter = 0;

        while (lastIndex != -1){

            lastIndex = str.indexOf(check, index);
            if (lastIndex != -1){
                counter++;
                index = lastIndex+check.length();
            }
        }

        return counter;

    }

    //заменить 1,2,3   на один,два,три
    public static String replaceNumber(String str) {
        if (str == null) {
            throw new IllegalArgumentException("String is null");
        }
            String replaced = str;
            replaced = replaced.replace("1", "один");
            replaced = replaced.replace("2", "два");
            replaced = replaced.replace("3", "три");
            return replaced;
    }

    //удалить каждый второй элемент
    public static void deleteEverySecondElement(StringBuilder str){
        if (str == null) {
            throw new IllegalArgumentException("String is null");
        }
        for (int i = 1; i < str.length(); i++){
            str.deleteCharAt(i);
        }
    }

}
