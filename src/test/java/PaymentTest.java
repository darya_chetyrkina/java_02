import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PaymentTest {

    @Test
    public void testSumInvalid() throws Exception {
        Assertions.assertThrows(Exception.class, () -> {
            Payment payment = new Payment("name", 12, 12, 2020, -1);
        });
    }

    @Test
    public void testNameInvalid() throws Exception {
        Assertions.assertThrows(Exception.class, () -> {
            Payment payment = new Payment(null, 12, 12, 2020, 100);
        });
    }
}