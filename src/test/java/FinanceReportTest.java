import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FinanceReportTest {

    @ Test
    void testFinanceReport() {
        Assertions.assertThrows(Exception.class, () -> {
            FinanceReport r = new FinanceReport("name", -1,12, 12, 2012);
        });
    }

    @ Test
    void testFinanceReport2() {
        Assertions.assertThrows(Exception.class, () -> {
            FinanceReport r = new FinanceReport(null, 1,12, 12, 2012);});
    }

    @Test
    void testSetPayments() {
        Assertions.assertThrows(Exception.class, () -> {
            FinanceReport r = new FinanceReport("name", 1,12, 12, 2012);
            Payment payment = new Payment("name", 15,12, 2021, 24531);
            r.setPayment(-1, payment);
        });
    }

    @Test
    void testGetPayment() {
        Assertions.assertThrows(Exception.class, () -> {
        FinanceReport report = new FinanceReport("name", 0, 12, 12, 2012);
        report.getPayment(1);});

    }

    @Test
    void testGetPayment2() {
        FinanceReport report = new FinanceReport("name", 2, 12, 12, 2012);
        Payment payment = new Payment("name", 15,12, 2021, 24531);
        report.setPayment(0, payment);
        assertEquals(payment, report.getPayment(0));

    }

    @Test
    void testSetPayment2() {
        FinanceReport report = new FinanceReport("name", 2,12, 12, 2012);
        Payment payment = new Payment("name", 15,12, 2021, 24531);
        report.setPayment(0, payment);
        payment.setSumPayment(10000);
        assertEquals(payment, report.getPayment(0));
    }

    @Test
    void testToString() {
        FinanceReport report = new FinanceReport("name", 2, 12, 12, 2012);
        Payment payment = new Payment("name", 15,12, 2021, 24531);
        Payment payment2 = new Payment ("name", 20,12, 2021, 10000);
        report.setPayment(0, payment);
        report.setPayment(1, payment2);
        //System.out.print(report);
    }

    @Test
    void testCopyFinanceReport() {
        FinanceReport report = new FinanceReport("name", 2, 12, 12, 2012);
        Payment payment = new Payment("name", 15, 12, 2021, 24531);
        Payment payment2 = new Payment("name", 0, 0, 0, 0);
        report.setPayment(0, payment);
        report.setPayment(1, payment2);
        FinanceReport report2 = new FinanceReport(report);
        report2.setPayment(0, payment2);
        assertNotEquals(report.getPayment(0), report2.getPayment(0));
    }

}