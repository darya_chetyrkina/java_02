import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FinanceReportProcessorTest {

    @Test
    void getReportByCh() {
        FinanceReport report = new FinanceReport("name", 2,12, 12, 2012);
        Payment payment1 = new Payment("A", 15,12, 2021, 24531);
        report.setPayment(0, payment1);
        Payment payment2 = new Payment("B", 20,12, 2021, 10000);
        report.setPayment(1, payment2);
        FinanceReport testReport = FinanceReportProcessor.getReportByCh('B', report);
        assertTrue(testReport.getQuantity() == 1 && testReport.getPayment(0).equals(report.getPayment(1)));
    }

    @Test
    void getReportBySum() {
        FinanceReport report = new FinanceReport("name", 2,12, 12, 2012);
        report.setPayment(0, new Payment("A", 15,12, 2021, 24531));
        report.setPayment(1, new Payment ("B", 20,12, 2021, 10000));
        FinanceReport testReport = FinanceReportProcessor.getReportBySum(report, 20000);
        assertTrue(testReport.getQuantity() == 1 && testReport.getPayment(0).equals(report.getPayment(1)));

    }

}