import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringProcessorTest {

    @Test
    public void TestRepeatNTimes() {
        assertEquals("r12r12r12", StringProcessor.repeatNTimes("r12", 3));
    }

    @Test
    public void TestRepeat0Times() {
        assertEquals("",StringProcessor.repeatNTimes("r12", 0));
    }

    @Test
    public void TestRepeatNTimesException() {
        try {
            StringProcessor.repeatNTimes("r12", -3);
        } catch (Exception e) {
            assertEquals("N is negative",
                    e.getMessage());
        }
    }

    @Test
    public void TestRepeatNTimesEmpty() {
        String s = new String ("");
        assertEquals("", StringProcessor.repeatNTimes(s, 3));
    }

    @Test
    public void TestCountOccurrencesException() {
        String s1 = new String ("r12hello1 2fdksdlf4212k;sdf");
        String s2 = new String("");

        try {
            StringProcessor.countOccurrences(s1, s2);
        } catch (Exception e) {
            assertEquals("Wrong second string",
                    e.getMessage());
        }
    }


    @Test
    public void TestCountOccurrences() {
        String s1 = new String ("r1   2hello12fdksdlf4212k;sdf");
        String s2 = new String("12f");
        assertEquals(1,  StringProcessor.countOccurrences(s1, s2));
    }

    @Test
    public void TestCountOccurrences2() {
        String s1 = new String ("111111");
        String s2 = new String("1");
        assertEquals(6,  StringProcessor.countOccurrences(s1, s2));
    }

    @Test
    void TestReplaceNumberException(){
        try{
            StringProcessor.replaceNumber(null);
        }catch (Exception e){
            assertEquals("String is null", e.getMessage());
        }

    }

    @Test
    public void TestReplaceNumber() {
        assertEquals("rодиндваheтриlодин",  StringProcessor.replaceNumber("r12he3l1"));
    }

    @Test
    public void TestReplaceNumber2() {
        assertEquals("terfg",  StringProcessor.replaceNumber("terfg"));
    }

    @Test
    void deleteEverySecondElement() {
        StringBuilder s1 = new StringBuilder ("12121212");
        StringProcessor.deleteEverySecondElement(s1);
        assertEquals("1111", s1.toString());
    }
    @Test
    void deleteEverySecondElement2() {
        StringBuilder s1 = new StringBuilder ("1");
        StringProcessor.deleteEverySecondElement(s1);
        assertEquals("1", s1.toString());
    }
    @Test
    void deleteEverySecondElement3(){
        StringBuilder s1 = new StringBuilder ();
        StringProcessor.deleteEverySecondElement(s1);
        assertEquals("", s1.toString());
    }
    @Test
    void deleteEverySecondElement4(){
        try{
            StringBuilder s1 = null;
        }catch (Exception e){
            assertEquals("String is null", e.getMessage());
        }

    }
}